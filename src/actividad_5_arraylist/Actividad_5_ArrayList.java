/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actividad_5_arraylist;
import java.util.ArrayList;
import java.util.Scanner;
/**
 *
 * @author Jose
 */
public class Actividad_5_ArrayList {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        System.out.println("-----------------------------------------Ejercicio_1--------------------------------------------------");
        
        ArrayList<Integer> Blanco = new ArrayList<Integer>();
        ArrayList<Integer> Rojo = new ArrayList<Integer>();
        ArrayList<Integer> Verde = new ArrayList<Integer>();
        
        Blanco.add(0);
        Blanco.add(0);
        Blanco.add(0);
        
        Rojo.add(123);
        Rojo.add(123);
        Rojo.add(212);
        
        Verde.add(252);
        Verde.add(15);
        Verde.add(156);
        
        
       System.out.println("Colores");
       
       System.out.println("Opuesto del balco: ");
        System.out.println("Blanco: "+Blanco);
        Blanco.set(0, 1);
        Blanco.set(1, 1);
        Blanco.set(2, 1);
        System.out.println("El opuesto es:");
        System.out.println("Negro: "+Blanco);
        
        System.out.println("Opuesto del rojo: ");
        System.out.println("Rojo: "+Rojo);
        Rojo.set(0, 12);
        Rojo.set(1, 241);
        Rojo.set(2, 252);
        System.out.println("El opuesto es:");
        System.out.println("Cafe: "+Rojo);
        
        System.out.println("Opuesto del verde: ");
        System.out.println("Rojo: "+Verde);
        Verde.set(0, 185);
        Verde.set(1, 251);
        Verde.set(2, 125);
        System.out.println("El opuesto es:");
        System.out.println("Amarillo: "+Verde);
        
        System.out.println("-----------------------------------------Ejercicio_2_y_3--------------------------------------------------");
        
        Scanner sc = new Scanner(System.in);
        ArrayList<String> Alfabeto = new ArrayList<String>();
        ArrayList<String> Morse = new ArrayList<String>();
        
        Alfabeto.add("A");
        Alfabeto.add("B");
        Alfabeto.add("C");
        Alfabeto.add("D");
        Alfabeto.add("E");
        Alfabeto.add("F");
        Alfabeto.add("G");
        Alfabeto.add("H");
        Alfabeto.add("I");
        Alfabeto.add("J");
        Alfabeto.add("K");
        Alfabeto.add("L");
        Alfabeto.add("M");
        Alfabeto.add("N");
        Alfabeto.add("O");
        Alfabeto.add("P");
        Alfabeto.add("Q");
        Alfabeto.add("R");
        Alfabeto.add("S");
        Alfabeto.add("T");
        Alfabeto.add("U");
        Alfabeto.add("V");
        Alfabeto.add("W");
        Alfabeto.add("X");
        Alfabeto.add("Y");
        Alfabeto.add("Z");
        
        Morse.add(".-");
        Morse.add("-...");
        Morse.add("-.-.");
        Morse.add("-..");
        Morse.add(".");
        Morse.add("..-.");
        Morse.add("--.");
        Morse.add("...");
        Morse.add("..");
        Morse.add(".---");
        Morse.add("-.-");
        Morse.add(".-..");
        Morse.add("--");
        Morse.add("-.");
        Morse.add("---");
        Morse.add(".--.");
        Morse.add("--.-");
        Morse.add(".-.");
        Morse.add("...");
        Morse.add("-");
        Morse.add("..-");
        Morse.add("...-");
        Morse.add(".--");
        Morse.add("-..-");
        Morse.add("-.--");
        Morse.add("--..");
        String pal = null;
    
        System.out.println("");
        ac1(Alfabeto,Morse);
        System.out.println("");
        System.out.println("Segundo ejercicio");
        System.out.println("\nIngrese una palabra con las letras del alfabeto");
        pal = sc.nextLine();
        char[] ch = pal.toCharArray();
        int asciiValue;
        int i = 0;
        char cChar = ch[i];
        asciiValue = (int) cChar;
        System.out.println("");
        
        System.out.println("-----------------------------------------Ejercicio_4--------------------------------------------------");
        
        ArrayList<String> aOctavos = new ArrayList<String>();

        aOctavos.add("América");
        aOctavos.add("Chivas");
        aOctavos.add("Leon");
        aOctavos.add("Cruz Azul");
        aOctavos.add("Toluca");
        aOctavos.add("Tigres");
        aOctavos.add("Monterrey");
        aOctavos.add("Pachuca");

        ArrayList<Integer> gol1 = new ArrayList<Integer>();

        gol1.add(4);
        gol1.add(6);
        gol1.add(3);
        gol1.add(2);
        gol1.add(1);
        gol1.add(2);
        gol1.add(3);
        gol1.add(2);

        ArrayList<String> aCuartos = new ArrayList<String>();

        aCuartos.add("América");
        aCuartos.add("Chivas");
        aCuartos.add("Cruz Azul");
        aCuartos.add("Tigres");

        ArrayList<Integer> gol2 = new ArrayList<Integer>();
        gol2.add(2);
        gol2.add(3);
        gol2.add(2);
        gol2.add(1);

        ArrayList<String> aSemifinal = new ArrayList<String>();

        aSemifinal.add("América");
        aSemifinal.add("Chivas");
        aSemifinal.add("Cruz Azul");
        aSemifinal.add("Tigres");

        ArrayList<Integer> gol3 = new ArrayList<Integer>();
        gol3.add(4);
        gol3.add(1);
        gol3.add(3);
        gol3.add(2);
        
        ac4(gol1, gol2, gol3, aOctavos, aCuartos, aSemifinal);
    }

    private static void ac1(ArrayList<String> Alfabeto, ArrayList<String> Morse) {
        System.out.println("Letras del alfabeto y su forma morse");
          
       System.out.println(Alfabeto.get(0)+" = "+Morse.get(0));
       System.out.println(Alfabeto.get(1)+" = "+Morse.get(1));
       System.out.println(Alfabeto.get(2)+" = "+Morse.get(2));
       System.out.println(Alfabeto.get(3)+" = "+Morse.get(3));
       System.out.println(Alfabeto.get(4)+" = "+Morse.get(4));
       System.out.println(Alfabeto.get(5)+" = "+Morse.get(5));
       System.out.println(Alfabeto.get(6)+" = "+Morse.get(6));
       System.out.println(Alfabeto.get(7)+" = "+Morse.get(7));
       System.out.println(Alfabeto.get(8)+" = "+Morse.get(8));
       System.out.println(Alfabeto.get(9)+" = "+Morse.get(9));
       System.out.println(Alfabeto.get(10)+" = "+Morse.get(10));
       System.out.println(Alfabeto.get(11)+" = "+Morse.get(11));
       System.out.println(Alfabeto.get(12)+" = "+Morse.get(12));
       System.out.println(Alfabeto.get(13)+" = "+Morse.get(13));
       System.out.println(Alfabeto.get(14)+" = "+Morse.get(14));
       System.out.println(Alfabeto.get(15)+" = "+Morse.get(15));
       System.out.println(Alfabeto.get(16)+" = "+Morse.get(16));
       System.out.println(Alfabeto.get(17)+" = "+Morse.get(17));
       System.out.println(Alfabeto.get(18)+" = "+Morse.get(18));
       System.out.println(Alfabeto.get(19)+" = "+Morse.get(19));
       System.out.println(Alfabeto.get(20)+" = "+Morse.get(20));
       System.out.println(Alfabeto.get(21)+" = "+Morse.get(21));
       System.out.println(Alfabeto.get(22)+" = "+Morse.get(22));
       System.out.println(Alfabeto.get(23)+" = "+Morse.get(23));
       System.out.println(Alfabeto.get(24)+" = "+Morse.get(24));
       System.out.println(Alfabeto.get(25)+" = "+Morse.get(25));
    }
    public static void ac2(char[] ch, int asciiValue) {

        Scanner sc = new Scanner(System.in);

        for (Character c : ch) {

            System.out.println("La letra " + (char) (c + 0) + " = " + (c + 0));

        }
    }
        
         private static void ac4(ArrayList<Integer> gol1, ArrayList<Integer> gol2, ArrayList<Integer> gol3, ArrayList<String> aOctavos, ArrayList<String> aCuartos, ArrayList<String> aSemifinal) {
        Scanner sc = new Scanner(System.in);
        if (gol1.get(0) > gol1.get(7)) {
            System.out.println("Resultados de octavos de final");
            System.out.println(aOctavos.get(0) + " es el ganador contra " + aOctavos.get(7) + " con " + gol1.get(0) + " goles");
        } else {
            System.out.println(aOctavos.get(7) + " es el ganador contra " + aOctavos.get(0) + " con " + gol1.get(7) + " goles");
        }

        if (gol1.get(1) > gol1.get(6)) {
            System.out.println(aOctavos.get(2) + " es el ganador contra " + aOctavos.get(6) + " con " + gol1.get(1) + " goles");
        } else {
            System.out.println(aOctavos.get(6) + " es el ganador contra " + aOctavos.get(1) + " con " + gol1.get(6) + " goles");
        }
        
        if (gol1.get(2) > gol1.get(5)) {
            System.out.println(aOctavos.get(2) + " es el ganador contra " + aOctavos.get(5) + " con " + gol1.get(2) + " goles");
        } else {
            System.out.println(aOctavos.get(2) + " es el ganador contra " + aOctavos.get(2) + " con " + gol1.get(5) + " goles");
        }

        if (gol1.get(3) > gol1.get(4)) {
            System.out.println(aOctavos.get(3) + " es el ganador contra " + aOctavos.get(4) + " con " + gol1.get(3) + " goles");
        } else {
            System.out.println(aOctavos.get(4) + " es el ganador contra " + aOctavos.get(3) + " con " + gol1.get(4) + " goles");
        }

        System.out.println("");
        if (gol2.get(0) > gol2.get(3)) {
            System.out.println("Resultados de cuarto de final");
            System.out.println(aCuartos.get(0) + " es el ganador contra " + aCuartos.get(3) + " con " + gol2.get(0) + " goles");
        } else {
            System.out.println(aCuartos.get(3) + " es el ganador contra " + aCuartos.get(0) + " con " + gol2.get(3) + " goles");
        }
        if (gol2.get(1) > gol2.get(2)) {
            System.out.println(aCuartos.get(1) + " es el ganador contra " + aCuartos.get(2) + " con " + gol2.get(1) + " goles");
        } else {
            System.out.println(aCuartos.get(2) + " es el ganador contra " + aCuartos.get(1) + " con " + gol2.get(2) + " goles");
        }

        System.out.println("");
        if (gol3.get(0) > gol3.get(3)) {
            System.out.println("Resultados de semfinal");
            System.out.println(aSemifinal.get(0) + " es el ganador contra " + aSemifinal.get(3) + " con " + gol3.get(0) + " goles");
        } else {
            System.out.println(aSemifinal.get(3) + " es el ganador contra " + aSemifinal.get(0) + " con " + gol3.get(3) + " goles");
        }
        if (gol3.get(1) > gol3.get(2)) {
            System.out.println(aSemifinal.get(1) + " es el ganador contra " + aSemifinal.get(2) + " con " + gol3.get(1) + " goles");
        } else {
            System.out.println(aSemifinal.get(2) + " es el ganador contra " + aSemifinal.get(1) + " con " + gol3.get(2) + " goles");
        }

    }
  
}
